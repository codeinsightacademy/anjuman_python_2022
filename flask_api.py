from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import pymysql
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)

cors = CORS(app)


@app.route('/students', methods=['GET'])
def get():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='root',
                           password='', db='anjuman_college')

    cur = conn.cursor()

    # cur.execute(f"SELECT * FROM bike_details ORDER BY {id} DESC;")
    cur.execute("SELECT * FROM students")
    output = cur.fetchall()

    print(type(output))  # this will print tuple

    for rec in output:
        print(rec)

    # To close the connection
    conn.close()

    return jsonify(output)


@app.route('/students', methods=['DELETE'])
def deleteRecord():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='root',
                           password='', db='anjuman_college')
    cur = conn.cursor()
    id = int(request.args.get('id'))
    # print(id)
    # quit()

    query = f"Delete from students WHERE id ={id}"
    res = cur.execute(query)
    conn.commit()
    print(cur.rowcount, "record(s) deleted")

    return {"result": "Record deleted Succesfully"}


@app.route('/students', methods=['POST'])
def insertRecord():
    conn = pymysql.connect(host='localhost', user='root',
                           password='', db='anjuman_college')

    # get raw json values
    raw_json = request.get_json()
    # print(raw_json)
    # quit()

    name = raw_json["name"]
    age = raw_json["age"]
    clas = raw_json["class"]
    city = raw_json["city"]

    sql = f"INSERT INTO students (name,age,city,class) VALUES ('{name}','{age}','{city}','{clas}')"
    print(sql)
    cur = conn.cursor()

    cur.execute(sql)
    conn.commit()
    return {"result": "Record inserted Succesfully"}


@app.route('/students', methods=['PUT'])
def updateRecord():
    conn = pymysql.connect(host='localhost', user='root',
                           password='', db='anjuman_college')

    raw_json = request.get_json()

    # print(type(raw_json));

    raw_json = request.get_json()
    id = raw_json['id']
    name = raw_json["name"]
    age = raw_json["age"]
    clas = raw_json["class"]
    city = raw_json["city"]


    sql_update_query = (
        f"UPDATE students SET name = '{name}',city ='{city}',age ='{age}',class = '{clas}' WHERE id = '{id}'")
    cur = conn.cursor()
    cur.execute(sql_update_query)
    conn.commit()
    return {"result": "Record updated Succesfully"}


if __name__ == "__main__":
    app.run(debug=True)
